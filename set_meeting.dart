import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:liam_witten_mtn/dashboard.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:liam_witten_mtn/list.dart';

class SetMeeting extends StatefulWidget {
  const SetMeeting({Key? key}) : super(key: key);

  @override
  State<SetMeeting> createState() => SetMeetingState();
}

class SetMeetingState extends State<SetMeeting> {
  static const String _title = 'Set Meeting';

  @override
  Widget build(BuildContext context) {
    TextEditingController locationController = TextEditingController();
    TextEditingController dateController = TextEditingController();
    TextEditingController timeController = TextEditingController();

    Future _setMeeting() {
      final location = locationController.text;
      final date = dateController.text;
      final time = timeController.text;

      final ref = FirebaseFirestore.instance.collection("Meetings").doc();

      return ref
          .set({
            "Location": location,
            "Date": date,
            "Time": time,
            "doc_ID": ref.id
          })
          .then((value) => {
                locationController.text = "",
                dateController.text = "",
                timeController.text = ""
              })
          .catchError((onError) => log(onError));
    }

    return Scaffold(
        appBar: AppBar(
          title: const Text(_title),
          centerTitle: true,
        ),
        body: ListView(
          children: [
            Column(
              children: [
                SizedBox(
                  height: 115,
                  width: 115,
                  child: Stack(
                    fit: StackFit.expand,
                    clipBehavior: Clip.none,
                    children: const [
                      CircleAvatar(
                        backgroundImage:
                            AssetImage("assets/Liam_circle_colour.png"),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8),
                  child: TextField(
                    controller: locationController,
                    style: const TextStyle(color: Colors.cyan),
                    decoration: const InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.cyan, width: 0.0)),
                        labelText: 'Location'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8),
                  child: TextField(
                    controller: dateController,
                    style: const TextStyle(color: Colors.cyan),
                    decoration: const InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.cyan, width: 0.0)),
                        labelText: 'Enter Date',
                        hintText: 'dd/mm/yyyy'),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8),
                  child: TextField(
                    controller: timeController,
                    style: const TextStyle(color: Colors.cyan),
                    decoration: const InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.cyan, width: 0.0)),
                        labelText: 'Enter Time',
                        hintText: '00:00'),
                  ),
                ),
                Container(
                    height: 60,
                    padding: const EdgeInsets.all(8),
                    child: ElevatedButton(
                      child: const Text('Set Meeting'),
                      onPressed: () => {
                        _setMeeting(),
                      },
                    )),
                Container(
                    alignment: Alignment.topRight,
                    padding: const EdgeInsets.all(10),
                    child: ElevatedButton(
                      child: const Text("Home"),
                      onPressed: () => {
                        Navigator.pop(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const Dashboard()))
                      },
                    ))
              ],
            ),
            const List()
          ],
        ));
  }
}

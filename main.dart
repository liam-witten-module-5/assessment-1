import 'package:flutter/material.dart';
import 'package:liam_witten_mtn/start_screen.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyBHB3_JKENpY38uVO6NlEOus8Og7fzsBnQ",
          authDomain: "mtn-app-academy-c5d43.firebaseapp.com",
          projectId: "mtn-app-academy-c5d43",
          storageBucket: "mtn-app-academy-c5d43.appspot.com",
          messagingSenderId: "466892865815",
          appId: "1:466892865815:web:bede3b93ce4e5237eab752",
          measurementId: "G-GWXX48NM0T"));

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'My App',
        home: const StartScreen(),
        theme: ThemeData(
            primarySwatch: Colors.cyan,
            hintColor: Colors.cyan,
            scaffoldBackgroundColor: Colors.black87));
  }
}

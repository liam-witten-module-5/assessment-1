import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

class List extends StatefulWidget {
  const List({Key? key}) : super(key: key);

  @override
  State<List> createState() => _ListState();
}

class _ListState extends State<List> {
  final Stream<QuerySnapshot> _myList =
      FirebaseFirestore.instance.collection("Meetings").snapshots();
  @override
  Widget build(BuildContext context) {
    TextEditingController locationUpdateController = TextEditingController();
    TextEditingController dateUpdateController = TextEditingController();
    TextEditingController timeUpdateController = TextEditingController();

    void _delete(docID) {
      FirebaseFirestore.instance
          .collection("Meetings")
          .doc(docID)
          .delete()
          .then((value) => "Meeting removed.");
    }

    void _update(data) {
      var collections = FirebaseFirestore.instance.collection("Meetings");

      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: const Text('Update'),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextField(
                      controller: locationUpdateController,
                      decoration: const InputDecoration(labelText: 'Location'),
                    ),
                    TextField(
                        controller: dateUpdateController,
                        decoration: const InputDecoration(
                            labelText: "Date", hintText: 'dd/mm/yyyy')),
                    TextField(
                        controller: timeUpdateController,
                        decoration: const InputDecoration(
                            labelText: "Time", hintText: '00:00')),
                    TextButton(
                        onPressed: () {
                          collections.doc(data['doc_ID']).update({
                            'Location': locationUpdateController.text,
                            'Date': dateUpdateController.text,
                            'Time': timeUpdateController.text
                          });
                          Navigator.pop(context);
                        },
                        child: const Text("Update"))
                  ],
                ),
              ));
    }

    return StreamBuilder(
      stream: _myList,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return const Text("ERROR!");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }
        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                      height: (MediaQuery.of(context).size.height),
                      width: (MediaQuery.of(context).size.width),
                      child: ListView(
                        children: snapshot.data!.docs
                            .map((DocumentSnapshot documentSnapshot) {
                          Map<String, dynamic> data =
                              documentSnapshot.data()! as Map<String, dynamic>;
                          return Column(
                            children: [
                              Card(
                                child: Column(
                                  children: [
                                    ListTile(
                                      textColor: Colors.black,
                                      title: Text(data['Location']),
                                      subtitle: Text(data['Date']),
                                    ),
                                    ButtonTheme(
                                        child: ButtonBar(
                                      children: [
                                        OutlinedButton.icon(
                                            onPressed: () => {_update(data)},
                                            icon: const Icon(Icons.edit),
                                            label: const Text('Edit')),
                                        OutlinedButton.icon(
                                            onPressed: () =>
                                                {_delete(data['doc_ID'])},
                                            icon: const Icon(Icons.delete),
                                            label: const Text('Remove'))
                                      ],
                                    ))
                                  ],
                                ),
                              )
                            ],
                          );
                        }).toList(),
                      )))
            ],
          );
        } else {
          return (const Text('No Data'));
        }
      },
    );
  }
}

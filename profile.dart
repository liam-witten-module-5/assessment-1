import 'package:flutter/material.dart';
import 'package:liam_witten_mtn/dashboard.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  static const String _title = 'Edit Profile';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(_title),
          centerTitle: true,
        ),
        body: Column(
          children: [
            SizedBox(
              height: 115,
              width: 115,
              child: Stack(
                fit: StackFit.expand,
                clipBehavior: Clip.none,
                children: const [
                  CircleAvatar(
                    backgroundImage:
                        AssetImage("assets/Liam_circle_colour.png"),
                  )
                ],
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(10),
              child: TextField(
                style: TextStyle(color: Colors.cyan),
                decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.cyan, width: 0.0)),
                    labelText: 'Old Username'),
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(10),
              child: TextField(
                style: TextStyle(color: Colors.cyan),
                decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.cyan, width: 0.0)),
                    labelText: 'New Username'),
              ),
            ),
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: ElevatedButton(
                  child: const Text("Change"),
                  onPressed: () => {},
                )),
            const Padding(
              padding: EdgeInsets.all(10),
              child: TextField(
                obscureText: true,
                style: TextStyle(color: Colors.cyan),
                decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.cyan, width: 0.0)),
                    labelText: 'Old Password'),
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(10),
              child: TextField(
                obscureText: true,
                style: TextStyle(color: Colors.cyan),
                decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.cyan, width: 0.0)),
                    labelText: 'New Password'),
              ),
            ),
            Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10),
                child: ElevatedButton(
                  child: const Text("Change"),
                  onPressed: () => {},
                )),
            Container(
                alignment: Alignment.bottomRight,
                padding: const EdgeInsets.all(10),
                child: ElevatedButton(
                  child: const Text("Home"),
                  onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Dashboard()))
                  },
                ))
          ],
        ));
  }
}

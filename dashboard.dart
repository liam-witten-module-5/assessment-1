import 'package:flutter/material.dart';
import 'package:liam_witten_mtn/set_meeting.dart';
import 'package:liam_witten_mtn/option3.dart';
import 'package:liam_witten_mtn/profile.dart';
import 'package:liam_witten_mtn/view_meeting.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  static const String _title = "Home Page";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(_title),
          centerTitle: true,
        ),
        body: Column(
          children: [
            SizedBox(
              height: 115,
              width: 115,
              child: Stack(
                fit: StackFit.expand,
                clipBehavior: Clip.none,
                children: const [
                  Padding(padding: EdgeInsets.all(10)),
                  CircleAvatar(
                      backgroundImage:
                          AssetImage("assets/Liam_circle_colour.png")),
                ],
              ),
            ),
            Container(
                height: 60,
                padding: const EdgeInsets.all(10),
                alignment: Alignment.center,
                child: ElevatedButton(
                  child: const Text("Set Meeting"),
                  onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const SetMeeting()))
                  },
                )),
            Container(
                height: 60,
                padding: const EdgeInsets.all(10),
                alignment: Alignment.center,
                child: ElevatedButton(
                  child: const Text("View Meeting"),
                  onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const ViewMeeting()))
                  },
                )),
            Container(
                height: 60,
                padding: const EdgeInsets.all(10),
                alignment: Alignment.center,
                child: ElevatedButton(
                  child: const Text("Option 3"),
                  onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Option3()))
                  },
                )),
            Container(
                height: 60,
                padding: const EdgeInsets.all(10),
                alignment: Alignment.center,
                child: ElevatedButton(
                  child: const Text("Profile"),
                  onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Profile()))
                  },
                ))
          ],
        ));
  }
}

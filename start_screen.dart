import 'package:flutter/material.dart';
import 'package:liam_witten_mtn/splash.dart';

class StartScreen extends StatefulWidget {
  const StartScreen({Key? key}) : super(key: key);

  @override
  State<StartScreen> createState() => _LoginState();
}

class _LoginState extends State<StartScreen> {
  static const String _title = 'Start';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(_title),
        centerTitle: true,
      ),
      body: Column(
        children: [
          SizedBox(
            height: 115,
            width: 115,
            child: Stack(
              fit: StackFit.expand,
              clipBehavior: Clip.none,
              children: const [
                CircleAvatar(
                  backgroundImage: AssetImage("assets/Liam_circle_colour.png"),
                )
              ],
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(8),
            child: TextField(
              style: TextStyle(color: Colors.cyan),
              decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.cyan, width: 0.0)),
                  labelText: 'Username'),
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(8),
            child: TextField(
              style: TextStyle(color: Colors.cyan),
              decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.cyan, width: 0.0)),
                  labelText: 'Email Address'),
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(8),
            child: TextField(
              obscureText: true,
              style: TextStyle(color: Colors.cyan),
              decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.cyan, width: 0.0)),
                  labelText: 'Password'),
            ),
          ),
          Container(
              height: 60,
              padding: const EdgeInsets.all(10),
              alignment: Alignment.center,
              child: ElevatedButton(
                child: const Text('Start'),
                onPressed: () => {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const SplashScreen()))
                },
              ))
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:liam_witten_mtn/dashboard.dart';
import 'package:liam_witten_mtn/list.dart';

class ViewMeeting extends StatelessWidget {
  const ViewMeeting({Key? key}) : super(key: key);

  static const String _title = "View meetings";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(_title),
          centerTitle: true,
        ),
        body: ListView(children: [
          Column(
            children: [
              SizedBox(
                  height: 115,
                  width: 115,
                  child: Stack(
                      fit: StackFit.expand,
                      clipBehavior: Clip.none,
                      children: const [
                        CircleAvatar(
                          backgroundImage:
                              AssetImage("assets/Liam_circle_colour.png"),
                        )
                      ])),
              Container(
                  alignment: Alignment.bottomRight,
                  padding: const EdgeInsets.all(10),
                  child: ElevatedButton(
                    child: const Text("Home"),
                    onPressed: () => {
                      Navigator.pop(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Dashboard()))
                    },
                  ))
            ],
          ),
          const List()
        ]));
  }
}
